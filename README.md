ics-ans-role-procserv
=============================

Ansible role to install procServ

Requirements
------------

- ansible >= 2.3
- molecule >= 1.24

Role Variables
--------------

```yaml
procserv_host: "localhost"
```

Example Playbook
----------------

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-procserv
```

License
-------

BSD 2-clause
