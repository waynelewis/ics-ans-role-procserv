import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


def test_procserv_paths(host):
    assert host.file('/var/log/procServ').exists
    assert host.file('/var/log/procServ').is_directory


def test_procserv_available(host):
    assert host.run_test("procServ")
